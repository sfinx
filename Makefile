#
#	Sfinx makefile
#

include Rules.Make

DIRS = lib sfinx faraon

.PHONY:	lib sfinx faraon clean push pull initdb todo dcp commit install tgz

all:		lib sfinx faraon

dist:		clean dep all

depend dep:
		@echo Making depend.
		@for dir in $(DIRS); do\
			(cd $$dir;$(MAKE) dep);\
		done

lib:		
		(cd lib; $(MAKE))

sfinx:		
		(cd sfinx; $(MAKE))

faraon:
		(cd faraon; $(MAKE))

clean:
		@echo Cleaning garbage.
		@for dir in $(DIRS); do\
			(cd $$dir;$(MAKE) clean);\
		done
		@rm -rf *out DEAD* errorlog.make *tgz

push:
		@git-push ssh://git@sfinx.od.ua/home/git/Harbour/sfinx

pull:
		@git-pull ssh://git@sfinx.od.ua/home/git/Harbour/sfinx

initdb:
		@echo -n "All Sfinx data ($(INSTALL_PREFIX)/share/sfinx/sfinx.fdb) will be DESTROYED ! Are you shure [Ny] ? "; \
		read answer; \
			case "$$answer" in \
			[yY]) 	echo "Deleting Database ..." ; \
				echo "TODO: check for running server !"; \
				rm -f $(INSTALL_PREFIX)/share/sfinx/sfinx.fdb; \
				echo "create database '$(INSTALL_PREFIX)/share/sfinx/sfinx.fdb' user 'sysdba' password 'harbour' page_size 16384 default character set koi8r;" > /tmp/create.sql ; \
				isql -ch koi8r -s 3 -q -i /tmp/create.sql; \
				rm -f /tmp/create.sql ; \
				isql -ch koi8r -s 3 -q -u sysdba -p harbour -i ./scripts/db.sql $(INSTALL_PREFIX)/share/sfinx/sfinx.fdb ; \
				echo Sfinx database initialized ;; \
			*)	exit;; \
			esac

commit:
		@git-diff
		@git-commit -a

# diff/commit/push

dcp:		commit	push

todo:		
		@setterm -foreground red
		@cat STATUS
		@echo
install:
		@strip -s sfinx/sfinx faraon/faraon
		cp -f sfinx/sfinx $(INSTALL_PREFIX)/bin
		cp -f faraon/faraon $(INSTALL_PREFIX)/bin

tgz:
		@strip -s sfinx/sfinx faraon/faraon
		@rm -rf /tmp/tgz/$(INSTALL_PREFIX)
		@mkdir -p /tmp/tgz/$(INSTALL_PREFIX)/bin
		@mkdir -p /tmp/tgz/$(INSTALL_PREFIX)/share/sfinx/etc
		@mkdir -p /tmp/tgz/$(INSTALL_PREFIX)/share/sfinx/doc
		@mkdir -p /tmp/tgz/$(INSTALL_PREFIX)/share/sfinx/backup
		@cp -f README STATUS LICENSE INSTALL CREDITS NEWS /tmp/tgz/$(INSTALL_PREFIX)/share/sfinx/doc
		@cp -f scripts/rc.sfinx /tmp/tgz/$(INSTALL_PREFIX)/share/sfinx/etc
		@cp -f sfinx/sfinx faraon/faraon scripts/faraon.sh /tmp/tgz/$(INSTALL_PREFIX)/bin
		@cp -a /usr/local/share/sfinx/libs /tmp/tgz/$(INSTALL_PREFIX)/share/sfinx
		@echo "create database '/tmp/tgz/$(INSTALL_PREFIX)/share/sfinx/sfinx.fdb' user 'sysdba' password 'harbour' page_size 16384 default character set koi8r;" > /tmp/create.sql
		@isql -ch koi8r -s 3 -q -i /tmp/create.sql
		@rm -f /tmp/create.sql
		@isql -ch koi8r -s 3 -q -u sysdba -p harbour -i ./scripts/db.sql /tmp/tgz/$(INSTALL_PREFIX)/share/sfinx/sfinx.fdb
		@tar cfzp sfinx-current.x86.tgz -C /tmp/tgz .
		@rm -rf /tmp/tgz
		
###
