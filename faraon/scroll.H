
 /*
  *   Copyright (C) 2007, Rus V. Brushkoff, All rights reserved.
  */

#ifndef Fl_Scroll_Resize_H_
#define Fl_Scroll_Resize_H_

#include <Fl/Fl_Scroll.H>

// Custom scroll that tells children to follow scroll's width when resized
class Fl_Scroll_Resize : public Fl_Scroll {

	public:
	// ���� ...
  bool resize_chld;
  void show() { resize_chld = true; Fl_Scroll::show(); }
  void hide() { resize_chld = false; Fl_Scroll::hide(); }
	Fl_Scroll_Resize(int X, int Y, int W, int H, const char* L=0) :
		Fl_Scroll(X,Y,W,H,L) { resize_chld = true; }
	void resize(int X, int Y, int W, int H) {
	      if (resize_chld) {
          // Tell children to resize to our new width
          for ( int t=0; t<children()-2; t++ ) {          // -2: skip scrollbars
              Fl_Widget *w = child(t);
              w->resize(X,Y,W, w->h());    // W-20: leave room for scrollbar
          }
          // Tell scroll children changed in size
          init_sizes();
//        init_scrollbars();
      }
        Fl_Scroll::resize(X,Y,W,H);
    }
};

#endif
