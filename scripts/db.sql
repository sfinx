
 /*
  *   Copyright (C) 2007, Rus V. Brushkoff, All rights reserved.
  */

create table info (
  schema_version INTEGER not null
);

insert into info (schema_version) values (0);

create table objects (
  id INTEGER not null,
  name VARCHAR(64) not null,
  description VARCHAR(256) not null,
  /* TODO: ��� ������� � ��������� ����� � �.�. */
  constraint pkobjects PRIMARY KEY (id),
  UNIQUE (name)    
);

insert into objects (id, name, description) values (1, 'slice', 'User defined abstraction');
insert into objects (id, name, description) values (2, 'file', 'Any file, containing some data');
insert into objects (id, name, description) values (3, 'note', 'Note about smth.');
insert into objects (id, name, description) values (4, 'event', 'Event');
insert into objects (id, name, description) values (5, 'contact', 'Contact');
insert into objects (id, name, description) values (6, 'task', 'Task or plan');
insert into objects (id, name, description) values (7, 'message', 'Mail or IM');
insert into objects (id, name, description) values (8, 'knowledge', 'Knowledge / kh [ know how ]');

/*  TODO: rule / intention / aim / dream */

create sequence slice_id;
set generator slice_id to 0;

/* slices - ������� ���������/������/����������. Slice ���������� �� ���������
  �������� ������������ ������� �����, �.�. �������� ����������� [ Dao ] �
  ����� ������������ ��� ��-����. ������ ����� ����������� ���������, ���� ���
  �������������� ������������

 */

/* TODO: add cascade delete to all trees */

create table slices (
  id BIGINT not null,
  parent_id BIGINT not null,
  name VARCHAR(64) not null,
  description VARCHAR(256) not null,
  /* ���������� ��� ��������������� ������ */
  directory VARCHAR(256),
  /* ���� �������� */
  ctime timestamp default 'now',
  /* ���� ��������� */
  etime timestamp default 'now',
  constraint pkslices PRIMARY KEY (id),
  UNIQUE (parent_id, name)
);

create index slices_etime on slices(etime);
create index slices_ctime on slices(ctime);

set term ^ ;

/* ������������� �������: firebird >= 2.x ������ */

create trigger slice_trg for slices
active before insert or update position 0
as
begin
  if (new.id is null) then
    new.id = gen_id(slice_id, 1);
  select cast('now' as timestamp) from rdb$database into new.etime;
end
^

set term ^ ;

insert into slices (parent_id, name, description) values (0, 'Root', 'Dao Slice');
insert into slices (parent_id, name, description, directory) values (1, 'Texts', '������, ����', 'Texts');
insert into slices (parent_id, name, description, directory) values (1, 'Images', '��������', 'Images');
insert into slices (parent_id, name, description, directory) values (1, 'Music', '������, MP3', 'Music');

/* module_conf - ��������� ������� */

create table files_module_conf (
  unsorted_files_path VARCHAR(256),
  sorted_file_tree VARCHAR(256),
  desc_file_name VARCHAR(256),
  desc_file_enable SMALLINT,
  detect_mime_type SMALLINT
);

insert into files_module_conf (unsorted_files_path, sorted_file_tree, desc_file_name, desc_file_enable) values ('/Arhiv/Unsorted', '/Arhiv/Sfinx', 'description.txt', 1);

create sequence file_id;
set generator file_id to 0;

create table files (

  id BIGINT not null,
  /* file name */
  name VARCHAR(256) not null,
  title VARCHAR(1024),
  /* �����/������������ */
  authority VARCHAR(512),
  description VARCHAR(1024),
  comments VARCHAR(1024),

  /* TODO : */
  /* is_directory SMALLINT default 0, */
  /* ���� ���� ����� � �����-���� ���������� */
  /* parent_file_id INTEGER default 0, */

  /* ���� ��������� ����� � ���� */
  ctime timestamp default 'now',
  /* ���� ��������� ���� � ����� � ���� */
  etime timestamp default 'now',

  /* ��� �������� ������� (main object) � ������� ������������ ���� */
  mtag_type INTEGER not null,
  /* ��� id */
  mtag_id BIGINT not null,
  /* ����� �������� �������� ����� ���� ������� �����-��������
     ������������ ��� ������� ������� ������ ������
     ������������ - �� ����� - ����� ��������� �� ����, ���� ������
   */
  mtag_slice_id BIGINT not null,

  fsize BIGINT default 0,
  sha256 VARCHAR(64),
  fsize_compressed BIGINT default 0,
  sha256_compressed VARCHAR(64),

  /* ����� ��������� �������� �����. ����� */
  csum_last_checked timestamp default '01.01.1900 00:00:00',
  csum_valid SMALLINT default 0,

  /* ��� �������� ����� :
    0 ��� - ������
    1 ��� - csum
  */
  /* check_type SMALLINT default 0, */

  store_in_sorted_location SMALLINT default 0,

  /* mimetype INTEGER default 0, */

  /* mime ��� ��� �������� � ����������� ���� */
  /* compress_mimetype INTEGER default 0, */
  /* full_index SMALLINT default 0, */
  /* added_by_uid INTEGER not null */

  constraint pkfiles PRIMARY KEY (id),
  UNIQUE (mtag_id, mtag_type, name)
);

create index files_etime on files(etime);
create index files_ctime on files(ctime);
create index files_name on files(name);
create index files_title on files(title);
create index files_auth on files(authority);
create index files_desc on files(description);
create index files_comm on files(comments);

set term ^ ;

create trigger file_trg for files
active before insert or update position 0
as
begin
  if (new.id is null) then
    new.id = gen_id(file_id, 1);
  select cast('now' as timestamp) from rdb$database into new.etime;
end
^

set term ^ ;

/* ���-������� ����� ������ � �������� */

create table file_tags (
  file_id BIGINT,
  obj_id BIGINT,
  obj_type INTEGER,
  constraint pkfile_slices PRIMARY KEY (file_id, obj_id, obj_type)
);

create sequence note_id;
set generator note_id to 0;

create table notes (

  id BIGINT not null,
  name VARCHAR(256) not null,
  url VARCHAR(256),
  text VARCHAR(4096),

  /* ���� ��������� � ���� */
  ctime timestamp default 'now',
  /* ���� ��������� ���� � ���� */
  etime timestamp default 'now',

  mtag_type INTEGER not null,
  mtag_id BIGINT not null,
  mtag_slice_id BIGINT not null,

  /* text field is crypted */
  secured SMALLINT default 0,

  constraint pknotes PRIMARY KEY (id),
  UNIQUE (mtag_slice_id, mtag_type, mtag_id, name)
);

create index notes_etime on notes(etime);
create index notes_ctime on notes(ctime);
create index notes_name on notes(name);
create index notes_url on notes(url);

/* ���� � ������� text crypted - ������ �� �����: ��������� ���� �� �����
   ����� ��� �������� ������ - ������������� ������ ����� ������� �� �������
   �����-���� ����� � ������� row ? */
/* create index notes_text on notes(text); */

set term ^ ;

create trigger note_trg for notes
active before insert or update position 0
as
begin
  if (new.id is null) then
    new.id = gen_id(note_id, 1);
  select cast('now' as timestamp) from rdb$database into new.etime;
end
^

set term ^ ;

insert into notes (name, url, text, mtag_id, mtag_type, mtag_slice_id) values ('sample note', 'some name', 'some url', 3, 1, 3);

/* ���-������� note-������ */

create table note_tags (
  note_id BIGINT,
  obj_id BIGINT,
  obj_type INTEGER,
  constraint pknote_slices PRIMARY KEY (note_id, obj_id, obj_type)
);

commit;

/*  TODO:

 - ������� ������
 - ������� ��������� ��� �������� (files/notes/events/contacts/etc.)
 - ������� ���������� ��� ������ (���. ���������/������ ����/etc.)

*/
