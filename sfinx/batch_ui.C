
 /*
  *   Copyright (C) 2007, Rus V. Brushkoff, All rights reserved.
  */

#include <sfinx.H>

void batch_ui::init()
{
}

void batch_ui::run()
{
 log("sfinx:run", "Starting batch sfinx jobs ...");
 sfinx->load_module_conf(SFINX_FILES_MODULE_CONF_REQUEST);
 while(1)
   idle();
}

void batch_ui::alert(bool fatal, const char *fmt, ...)
{
 char buf[10240];
 ::va_list args;
 ::va_start(args, fmt);
 ::va_end(args);
 ::vsnprintf(buf, sizeof(buf), fmt, args);
// alert_txt->label(buf);
// alert_w->show();
// alert_w->take_focus(alert_button);
// f_text_display->top(alert_w);
// alert_button->user_data((void *)fatal);
}
