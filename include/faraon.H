
 /*
  *   Copyright (C) 2007, Rus V. Brushkoff, All rights reserved.
  */

#ifndef _FARAON_H_
#define _FARAON_H_

#include <F_App.H>
#include <string>
#include <FL/Fl_Preferences.H>
#include <cc++/socket.h>
#include <network.H>
#include <version.H>

using namespace F;
using namespace ost;
using namespace std;

#define SFINX_LOG_DEBUG   0
#define SFINX_LOG_NOTICE  1
#define SFINX_LOG_WARN    2
#define SFINX_LOG_ERROR   3
#define SFINX_LOG_FATAL   4

class app_specific_ui {

  Mutex big_lock;

 public:

  app_specific_ui() { }
  virtual ~app_specific_ui() { }
  virtual void log(int level, const char *fmt, ...) = 0;
  virtual void edit_slice(sfinx_slice_t *slice) = 0;
  virtual void fill_preferences() = 0;
  virtual void fill_unsorted_files_tree(sfinx_files_vector_t *) = 0;
  virtual void progress(sfinx_progress_t *) = 0;
  virtual void files_module_edit_reply(sfinx_string_t *) = 0;
  virtual void files_module_edit(sfinx_file_t *) = 0;
  virtual void notes_module_reply(sfinx_string_t *) = 0;
  virtual void notes_module_edit(sfinx_note_t *) = 0;
  virtual void alert(sfinx_string_t *) = 0;
  virtual void search_browser(sfinx_pair_vector_t *) = 0;
  virtual void fill_objects_tree() = 0;
  void lock() { big_lock.enterMutex(); }
  void unlock() { big_lock.leaveMutex(); }
};

class x11_ui: public F_FLTK_UI, public app_specific_ui {
 public:
   x11_ui() : F_FLTK_UI() { }
   ~x11_ui() { }
   void init();
   void log(int level, const char *fmt, ...);
   void edit_slice(sfinx_slice_t *slice);
   void fill_preferences();
   void fill_unsorted_files_tree(sfinx_files_vector_t *);
   void progress(sfinx_progress_t *);
   void files_module_edit_reply(sfinx_string_t *);
   void files_module_edit(sfinx_file_t *);
   void notes_module_edit(sfinx_note_t *);
   void notes_module_reply(sfinx_string_t *);
   void alert(sfinx_string_t *);
   void search_browser(sfinx_pair_vector_t *);
   
   void fill_objects_tree();
};

class faraon_app : public F_App {
  // available ui's for this app
  x11_ui ui1;

 public:

  connection_to_sfinx sfinx;

  string server_name;
  sfinx_pair_vector_t files_module_conf;
  sfinx_slice_vector_t slices; // ������ ������� � ���������, � ������� ����� ������
  bool slices_need_update;

  faraon_app(int argc, char **argv, const char *name = 0,
    const char *version = 0, const char *author = 0,
  	const char *license = 0, CommandOption *opts = 0) :
  	  F_App(argc, argv, name, version, author, license, opts) {
  	    files_module_conf.tid(SFINX_FILES_MODULE_CONF);
  	    files_module_conf.add(FILES_MODULE_UNSORTED_PATH, new sfinx_string_t);
  	    files_module_conf.add(FILES_MODULE_SORTED_TREE_PATH, new sfinx_string_t);
        files_module_conf.add(FILES_MODULE_DESC_FILE_ENABLE, new sfinx_8bit_t);
        files_module_conf.add(FILES_MODULE_DESC_FILE_NAME, new sfinx_string_t);
        slices_need_update = true;
 }
  app_specific_ui *ui() { return dynamic_cast<app_specific_ui *>(ui_); }
	~faraon_app() { }
	void read_preferences();
	void write_preferences();
	bool connect();
	bool connected() { return sfinx.connected(); }
	void main();
	bool refresh_slices(bool force = false) {
	  if (force || slices_need_update)
	    return sfinx.send(SFINX_OBJECTS_TREE_REQUEST, (u32_t)0xFFFFFFFF);
	  ui()->fill_objects_tree();
	  return false;
 }
  void prepare_search_info(sfinx_t *el, string &inf);
	bool process_requests();
	string slice_name(sfinx_id_t slice_id) {
	  sfinx_slice_t *slice = slices.find(slice_id);
	  string t;
	  if (!slice)
	    return t;
	  t = slice->name();
	  t += " [ ";
	  t += slice->description();
	  t += " ]";
	  return t;
 }
  string slice_hierarchy(sfinx_id_t sid) {
    vector <string> h;
    sfinx_slice_t *slice;
    do {
      slice = slices.find(sid);
      if (!slice)
        break;
      string t = slice->name();
      t += " [ ";
      t += slice->description();
      t += " ]";
      sid = slice->parent_id;
      h.push_back(t);
  } while (sid != 1);
   string res;
   if (h.size()) {
     for (int32_t i = h.size() - 1; i >= 0; i--) {
       res.append(h[i]);
       if (i)
         res.append(", ");
     }
   }
    return res;
  }
  string slice_hierarchy_path(sfinx_id_t sid) {
    vector <string> h;
    sfinx_slice_t *slice;
    do {
      slice = slices.find(sid);
      if (!slice)
        break;
      h.push_back("/" + slice->directory_);
      sid = slice->parent_id;
  } while (sid != 1);
   string res;
   if (h.size()) {
     for (int32_t i = h.size() - 1; i >= 0; i--) {
       res.append(h[i]);
     }
   }
    return res;
  }
};

extern faraon_app *app;

#endif
