
 /*
  *   Copyright (C) 2007, Rus V. Brushkoff, All rights reserved.
  */

#ifndef _CRYPTER_H_
#define _CRYPTER_H_

#include <netinet/in.h>
#include <elements.H>
#include <unistd.h>
#include <gcrypt.h>

#define SFINX_HASH_BUF_SIZE  65536

class sfinx_hash_t {
  int algo;
  u32_t len;
  gcry_md_hd_t hd;
  gcry_error_t err;
  u8_t *buf;
  string strval;
  public :
    sfinx_hash_t(int alg) {
      algo = alg;
      len = gcry_md_get_algo_dlen(algo);
      err = gcry_md_open (&hd, algo, 0);
      if (err)
        throw("Can't open hash algorithm !");
      buf = new u8_t[SFINX_HASH_BUF_SIZE];
   }
    ~sfinx_hash_t() {
      delete [] buf;
      gcry_md_close(hd);
   }
    const char *strvalue() {
      char *p = (char *) gcry_md_read(hd, algo);
      strval.clear();
      for (u32_t i = 0; i < len; i++) {
        char b[16];
        sprintf(b, "%02hhx", p[i]);
        strval += b;
     }
      return strval.c_str();
   }
    void *value() { return gcry_md_read(hd, algo); }
    bool hash(const string &file) { return hash(file.c_str()); }
    bool hash(const char *file) {
      ifstream f(file, ios::in | ios::binary);
      if (!f) {
        debug("Error opening %s", file);
        return true;
     }
      gcry_md_reset(hd);
      while (!f.eof()) {
        f.read((char *)buf, SFINX_HASH_BUF_SIZE);
        if (!f.eof() && !f) {
          debug("Error reading %s", file);
          return true;
       }
        gcry_md_write(hd, buf, f.gcount());
      }
       f.close();
       // debug("%s", strvalue());
       return false;
   }
};

class sfinx_crypter {
   u8_t header_[5 + 1 + 4]; // magic + type + next data len
   u8_t header_len_;
   u8_t type_;
protected:
   virtual int crypt(u32_t size, u8_t **b) = 0;
   virtual int decrypt(u32_t size, u8_t **b) = 0;
 public:
   static bool get_random_bytes(sfinx_8bit_vector_t &buf, u32_t len) {
     // get some data from /dev/urandom - somewhat insecure
     int fd = open("/dev/urandom", O_RDONLY);
     if (fd < 0)
       return 1;
     u8_t byte;
     while (len--) {
       if (read(fd, &byte, 1) != 1)
         break;
       buf.add(byte);
    }
     close(fd);
     return len;
   }
   sfinx_crypter(u8_t t) {
     strcpy((char *)header_, SFINX_PACKET_MAGIC);
     header_[5] = type_ = t;
     header_len_ = 6;
  }
   virtual ~sfinx_crypter() { }
   u8_t type() { return type_; }
   u8_t header_len() { return header_len_; }
   u8_t *header() { return header_; }
   bool decrypt(sfinx_elements &buf) { return false; }
   bool crypt(sfinx_elements &buf) {
     // set header len
     header_len_ = 6;
     s8_t data_size_len = -1;
     // crypt(buf, len);
     u32_t coded_data_size_be32 = buf.code(buf.size(), &data_size_len);
     header_len_ += data_size_len;
     memcpy(header_ + 6, ((u8_t *)&coded_data_size_be32) + (sizeof(u32_t) - data_size_len), data_size_len);
    // debug("got %d elements, %d bytes", buf.n_elements, buf.data_size_);
     return 0;
   }
};

class sfinx_crypter_plain : public sfinx_crypter {
  public:
    sfinx_crypter_plain() : sfinx_crypter(SFINX_PACKET_PLAIN) { }
    int crypt(u32_t size, u8_t **b) { return 0; }
    int decrypt(u32_t size, u8_t **b) { return 0; }
};

class sfinx_crypter_crc32 : public sfinx_crypter {
  public:
    sfinx_crypter_crc32() : sfinx_crypter(SFINX_PACKET_CRC32) { }
    int crypt(u32_t size, u8_t **b) { return 1; }
    int decrypt(u32_t size, u8_t **b) { return 1; }
};

class sfinx_crypter_aes : public sfinx_crypter {
  public:
    sfinx_crypter_aes() : sfinx_crypter(SFINX_PACKET_CRYPTED_AES) { }
    int crypt(u32_t size, u8_t **b) { return 1; }
    int decrypt(u32_t size, u8_t **b) { return 1; }
};

class sfinx_crypter_rsa : public sfinx_crypter {
  public:
    sfinx_crypter_rsa() : sfinx_crypter(SFINX_PACKET_CRYPTED_RSA) { }
    int crypt(u32_t size, u8_t **b) { return 1; }
    int decrypt(u32_t size, u8_t **b) { return 1; }
};

#endif
