
 /*
  *   Copyright (C) 2007, Rus V. Brushkoff, All rights reserved.
  */

#ifndef _PLATFORM_H_
#define _PLATFORM_H_

// hmm, do not know how elegant this has to be solved in 64bit platforms
#define to_voidp(x)	((void *)((unsigned long)(x)))
#define from_voidp(x)	((unsigned long)(x))

#endif
